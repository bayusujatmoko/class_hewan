package Hewan;

public class IkanAirLaut extends Hewan {
    protected String habitat, nama;
    public IkanAirLaut(String nama, String habitat, int kaki, String alat_nafas) {
        super(kaki, alat_nafas);
        this.nama = nama;
        this.habitat = habitat;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setHabitat(String habitat) {
        this.habitat = habitat;
    }

    public String getNama() {
        return nama;
    }

    public String getHabitat() {
        return habitat;
    }

    @Override
    public void tampil() {
        System.out.println("Nama        : "+nama);
        System.out.println("habitat     : "+habitat);
        super.tampil();
    }
}
