package Hewan;

public class Main {
    public static void main(String[] args) {
        Ayam a = new Ayam(2,"paru-paru");
        Kucing k = new Kucing(4, "paru-paru");
        IkanAirTawar iat = new IkanAirTawar("lele","air tawar",0,"insang");
        IkanAirLaut ial = new IkanAirLaut("hiu","air laut",0,"insang");

        System.out.println("=== ciri - ciri Ayam ===");
        a.tampil();
        System.out.println("");
        System.out.println("=== ciri - ciri Kucing ===");
        k.tampil();
        System.out.println("");
        System.out.println("=== ciri - ciri Ikan ===");
        iat.tampil();
        System.out.println("");
        ial.tampil();
    }
}
