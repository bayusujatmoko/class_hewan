package Hewan;

public class Hewan {
    protected int kaki;
    protected String alat_nafas;

    public Hewan(int kaki,String alat_nafas){
        this.kaki = kaki;
        this.alat_nafas = alat_nafas;
    }

    public void tampil(){
        System.out.println("jumlah kaki : "+kaki);
        System.out.println("alat nafas  : "+alat_nafas);
    }
}
